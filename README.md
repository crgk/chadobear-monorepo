# chadobear: the repo

This is the monorepo for all my chadobear stuff.

- Smirk icon files are in "smirk-icon"
- Files I use to produce videos and streams are in "studio"
- My crossword app project is in "crossobear"
