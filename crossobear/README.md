# crossobear

I have decided to make my own crossword-solving app. I thought about forking crosshare, but I decided a) my changes probably wouldn't be welcomed upstream and b) I didn't want to learn the codebase. So I'll just mess around and make a bad one myself!

## Goals:
- Load puzzles from .puz files
- Display only the active and intersecting clues, nice and big
- Make puzzles harder, for fun

