import { range, publish } from './util.js'

const SVG_NS = "http://www.w3.org/2000/svg";
const XLINK_NS = "http://www.w3.org/1999/xlink";
const XHTML_NS = "http://www.w3.org/1999/xhtml";

export class Board {
    static svg: BoardSVG;

    static create(): BoardSVG {
        const b = document.createElementNS(SVG_NS, "svg");
        b.setAttribute("id", "board");
        b.setAttribute("viewBox", "-10 -10 1200 1200");
        b.setAttribute("width", "800");
        b.setAttribute("height", "800");
        b.setAttribute("xmlns", SVG_NS);
        Board.svg = b;
        return Board.svg;
    }

    static update(cursor: Cursor) {
        Tiles.delight();
        Tiles.highlight(cursor);
        Clues.delight();
        Clues.highlight(cursor);
    }
}

class GridLayer {
    static readonly type: string;
    // TODO: make these dynamic, sized down to fit larger grids
    static readonly elWidth = 100;
    static readonly elHeight = 100;
    static readonly elMargin = 4;

    static getElementX(n: number, offset = 0) {
        return (this.elWidth * n + offset).toString();
    }
    static getElementY(n: number, offset = 0) {
        return (this.elHeight * n + offset).toString();
    }
    static buildID(x: number, y: number) {
        return `${this.type}-${x}-${y}`;
    }
}

export class Tiles extends GridLayer {
    static readonly type = "tile";
    static readonly nxOffset = 10;
    static readonly nyOffset = 25;

    static create(grid: Square[][]) {
        let g = document.createElementNS(SVG_NS, 'g');
        g.setAttribute("id", "tiles");
        for (const r of range(grid.length)) {
            for (const c of range(grid[r].length)) {
                const t = document.createElementNS(SVG_NS, 'rect');
                t.setAttribute("id", this.buildID(r, c));

                t.setAttribute("x", Tiles.getElementX(c));
                t.setAttribute("y", Tiles.getElementY(r));
                t.setAttribute("rx", (Tiles.elWidth/3).toString());
                t.setAttribute("width", (Tiles.elWidth - Tiles.elMargin).toString());
                t.setAttribute("height", (Tiles.elHeight - Tiles.elMargin).toString());
                t.classList.add("tile");

                if (grid[r][c].type == "block") {
                    t.classList.add("block")
                } else {
                }
                g.appendChild(t);

                // Tile numbers
                if (grid[r][c].type == "cell" && grid[r][c].number) {
                    const n = document.createElementNS(SVG_NS, 'text');
                    n.setAttribute("id", `number-${grid[r][c].number}`);
                    n.setAttribute("x", Tiles.getElementX(c, Tiles.nxOffset));
                    n.setAttribute("y", Tiles.getElementY(r, Tiles.nyOffset));
                    n.classList.add("tile-number");
                    n.textContent = grid[r][c].number.toString();
                    g.appendChild(n);
                }
            }
        }
        // TODO: Fix the abuse of the Message type
        g.addEventListener("pointerup", e => {
            const coords = (e.target as Element).id.split("-").slice(1).map(Number);
            publish("select", {
                target: {row: coords[0], col: coords[1]}
            });
        });
        return g;
    }
    static highlight(c: Cursor) {

        // relatedBlanks includes the target
        const relatedBlanks = c.blank.entries[c.active].blanks;
        for (const blank of relatedBlanks) {
            const tile = document.getElementById(this.buildID(blank.cell.row, blank.cell.col));
            tile.classList.add("related");
        }

        const targetTile = document.getElementById(this.buildID(c.row, c.col));
        targetTile.classList.replace("related", "active");
    }
    static delight() {
        for (const t of document.getElementsByClassName("tile")) {
            t.classList.remove("active", "related");
        }
    }
}
export class Blanks extends GridLayer {
    static type = "blanks";

    static getElementX(n: number) {
        return ((this.elWidth * n) + this.elWidth/2).toString();
    }
    static getElementY(n: number) {
        return ((this.elHeight * n) + this.elHeight/2).toString();
    }

    static create(grid: Square[][]) {
        let g = document.createElementNS(SVG_NS, "g");
        g.setAttribute("id", "blanks");
        for (const r of range(grid.length)) {
            for (const c of range(grid[r].length)) {
                let t = document.createElementNS(SVG_NS, 'text');
                t.setAttribute("id", Blanks.buildID(r, c));

                t.setAttribute("x", Blanks.getElementX(c));
                t.setAttribute("y", Blanks.getElementY(r));

                t.classList.add("blank");
                t.setAttribute("text-anchor", "middle");
                t.setAttribute("dominant-baseline", "middle");
            
                g.appendChild(t);
            }
        }
        return g;
    }
    
    static enter(e: Blank) {
        const id = Blanks.buildID(e.cell.row, e.cell.col);
        const t = Board.svg.getElementById(id) as SVGTextElement;
        if (!e.correct) {  // TODO: && showErrors
            t.classList.add("incorrect");
        }
        t.textContent = e.text;
    }

    static clear(e: Blank ) {
        const id = Blanks.buildID(e.cell.row, e.cell.col);
        const t = Board.svg.getElementById(id);
        t.textContent = "";
        t.classList.remove("incorrect", "guess");
    }
}

export class Clues {

    static getClueTr(clue: Clue) {
        let cluetr = document.createElement("tr");
        cluetr.setAttribute("id", clue.id);
        cluetr.classList.add("clue-list-item");
        let cluenumtd = document.createElement("td");
        cluenumtd.innerText = clue.number.toString();
        let cluetexttd = document.createElement("td");
        cluetexttd.innerText = clue.text;
        cluetr.appendChild(cluenumtd);
        cluetr.appendChild(cluetexttd);
        return cluetr;
    }

    static setClues(clues: ClueCollection) {
        const acrosstable = document.getElementById("across-clues"),
              downtable = document.getElementById("down-clues");
        if (acrosstable === null || downtable === null) {
            return;
        }
        clues.forEach(c => {
            const table = c.direction == "across" ? acrosstable : downtable;
            table.appendChild(Clues.getClueTr(c));
        });
    }

    static highlight(cursor: Cursor) {
        const active = cursor.active;
        const entries = cursor.blank.entries;
        const a = document.getElementById(entries.across.id),
              d = document.getElementById(entries.down.id);
        if (active == "across") {
            a.classList.add("active");
            d.classList.add("related");
            this.updateHead(`${a.children[0].innerHTML}A`, a.children[1].innerHTML)
        } else {
            a.classList.add("related");
            d.classList.add("active");
            this.updateHead(`${d.children[0].innerHTML}D`, d.children[1].innerHTML)
        }
    }

    static updateHead(number: string, text: string) {
        const dt = document.getElementById("active-clue-number"),
              dd = document.getElementById("active-clue-text");
        dt.innerText = number;
        dd.innerText = text;
    }

    static delight() {
        for (const c of document.getElementsByClassName("clue-list-item")) {
            c.classList.remove("active");
            c.classList.remove("related");
        }
    }
}