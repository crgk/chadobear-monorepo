interface Cell {
    type: "cell"
    solution: string,
    row: number,
    col: number,
    across: number,
    down: number,
    number?: number,
}

interface PUZGridCell {
    solution: string,
    number?: number,
    across: number,
    down: number,
}

interface Block {
    type: "block"
}
type Square = Cell | Block;

type Direction = "across" | "down";

interface Cursor {
    row: number,
    col: number,
    blank: Blank,
    active: Direction,
}

interface Clue {
    id: string;
    direction: Direction;
    number: number;
    text: string;
    related?: Clue;
}

type EntryPair = {
    across: Entry,
    down: Entry
}

interface Blank {
    id: string;
    text: string;
    cell: Cell;
    correct: Boolean;
    guess: Boolean;
    entries: EntryPair;
}

type ClueCollection = Map<string, Clue>;

interface Entry {
    id: string;
    blanks: Blank[];
    clue: Clue;
    correct: Boolean;
}

interface Crossword {
    width: number,
    height: number,
    clues: ClueCollection,
    grid: Square[][],
    blanks: Map<string, Blank>,
    numEntries: number,
    entries: Map<string, Entry>,
}

interface GameState {
    cursor: Cursor,
    correct: number,
    solved: Boolean,
    puzzle: Crossword
}

// TODO: more message types
interface Message {
    from?: Cursor,
    direction?: string,
    value?: string,
    reverse?: Boolean,
    target?: {row: number, col: number},
}

type BoardSVG = SVGSVGElement;
