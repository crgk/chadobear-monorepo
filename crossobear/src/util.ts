export function range(n: number): number[] {
    return Array.from(Array(n).keys());
}

export function clearChildren(el: HTMLElement, keep = 0) {
    while (el.children.length > keep) {
        el.removeChild(el.lastChild);
    }
}

const subscriptions = {}
export function subscribe(command, callback) {
    let subs = subscriptions[command] ?? [];
    subs.push(callback);
    subscriptions[command] = subs;
}
export function publish(command, message: Message) {
    for (const sub of subscriptions[command]) {
        sub(message);
    }
}