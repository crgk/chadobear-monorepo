import { PUZtoJSON } from '../thirdparty/rschroll/loaders.js';
import { Board, Clues, Blanks, Tiles } from './dom.js';
import { clearChildren, range, publish, subscribe } from './util.js';
import { VirtualKeyboard } from './components/keyboard.js';
class CrosswordClue {
    static buildID(ad, n) {
        return `${ad}-${n}`;
    }
    constructor(direction, value, index) {
        this.direction = direction;
        this.text = value;
        this.number = index;
        this.id = CrosswordClue.buildID(this.direction, this.number);
    }
}
class CrosswordEntry {
    constructor(clue) {
        this.id = clue.id;
        this.clue = clue;
        this.blanks = [];
        this.correct = false;
    }
}
class CrosswordBlank {
    constructor(cell, entries) {
        return {
            id: `blank-${cell.row}-${cell.col}`,
            cell: cell,
            text: "",
            entries: entries,
            correct: false,
            guess: false
        };
    }
}
class CrosswordPuzzle {
    constructor(width, height, across, down, grid, numEntries) {
        this.width = width;
        this.height = height;
        this.numEntries = numEntries;
        this.clues = new Map();
        this.entries = new Map();
        this.blanks = new Map();
        // init entries
        for (const i of range(across.length)) {
            if (across[i] == null) {
                continue;
            }
            let c = new CrosswordClue("across", across[i], i);
            this.clues.set(c.id, c);
            let e = new CrosswordEntry(c);
            this.entries.set(c.id, e);
        }
        // init clues
        for (const i of range(down.length)) {
            if (down[i] == null) {
                continue;
            }
            const c = new CrosswordClue("down", down[i], i);
            this.clues.set(c.id, c);
            let e = new CrosswordEntry(c);
            this.entries.set(c.id, e);
        }
        this.grid = grid;
        // init blanks
        for (const row of grid) {
            for (const square of row) {
                if (square.type == "block") {
                    continue;
                }
                const ae = this.entries.get(`across-${square.across}`);
                const de = this.entries.get(`down-${square.down}`);
                const blank = new CrosswordBlank(square, { across: ae, down: de });
                this.blanks.set(blank.id, blank);
                ae.blanks.push(blank);
                de.blanks.push(blank);
            }
        }
    }
    static fromJson(puzzleJson) {
        return new CrosswordPuzzle(puzzleJson["ncol"], puzzleJson["nrow"], puzzleJson["across"], puzzleJson["down"], 
        // TODO: this is ugly
        puzzleJson["grid"].map((rows, i) => {
            return rows.map((cell, j) => {
                if (cell.type == "block") {
                    return cell;
                }
                cell;
                return {
                    type: "cell",
                    solution: cell.solution,
                    number: cell.number,
                    row: i,
                    col: j,
                    across: cell.across,
                    down: cell.down,
                };
            });
        }), puzzleJson["ncells"]);
    }
}
let gameState = {
    cursor: { row: 0, col: 0, active: "across", blank: null },
    correct: 0,
    solved: false,
    puzzle: null
};
function handleFileInput(event) {
    let file = event.target.files[0];
    let reader = new FileReader();
    reader.readAsArrayBuffer(file);
    file.arrayBuffer().then(loadPuzBuf);
}
function loadPuzBuf(buf) {
    try {
        let puzzleJson = PUZtoJSON(buf);
        // Instead, initialize the board based on this
        console.dir(puzzleJson);
        console.log(JSON.stringify(puzzleJson));
        let crosswordPuzzle = CrosswordPuzzle.fromJson(puzzleJson);
        initializePuzzle(crosswordPuzzle);
    }
    catch (e) {
        console.error(e);
    }
}
function initializePlaceholder() {
    fetch("a-tribute-to-my-cats.puz").then(resp => resp.arrayBuffer())
        .then(loadPuzBuf);
}
function initializePuzzle(crossword) {
    clearPuzzle();
    gameState.puzzle = crossword;
    const firstBlank = [...crossword.blanks.values()][0];
    gameState.cursor = {
        blank: firstBlank,
        row: firstBlank.cell.row,
        col: firstBlank.cell.col,
        active: "across",
    };
    setBoard(crossword);
    setClues(crossword);
    Board.svg.appendChild(Blanks.create(crossword.grid));
    Board.update(gameState.cursor);
}
function clearPuzzle() {
    let boardcontainer = document.getElementById("board-container");
    clearChildren(boardcontainer);
    let across = document.getElementById("across-clues");
    clearChildren(across, 1);
    let down = document.getElementById("down-clues");
    clearChildren(down, 1);
}
function setBoard(crossword) {
    const boardcontainer = document.getElementById("board-container");
    boardcontainer.appendChild(Board.create());
    Board.svg.appendChild(Tiles.create(crossword.grid));
    Board.svg.focus();
}
function setClues(crossword) {
    Clues.setClues(crossword.clues);
}
const movements = {
    "ArrowUp": "up",
    "ArrowRight": "right",
    "ArrowDown": "down",
    "ArrowLeft": "left",
};
function handleKeydown(event) {
    let command, message;
    if (Object.keys(movements).includes(event.code)) {
        command = "move";
        message = {
            from: gameState.cursor,
            direction: movements[event.code]
        };
    }
    else if (event.code === "Tab") {
        command = "next";
        message = {
            cursor: gameState.cursor,
            reverse: event.shiftKey
        };
    }
    else if (event.code === "Space") {
        command = "switch";
        message = { cursor: gameState.cursor };
    }
    else if (event.code.startsWith("Key")) {
        // TODO: I don't think this is reliable - other keys may start with Key
        command = "fill";
        message = {
            cursor: gameState.cursor,
            value: event.key.toUpperCase(),
        };
    }
    else if (event.code === "Backspace") {
        command = "backspace";
        message = { cursor: gameState.cursor };
    }
    else {
        command = "noop";
    }
    publish(command, message);
    event.preventDefault();
}
function move(message) {
    const nextCursor = Object.assign({}, message.from);
    if (message.direction === "up" && gameState.cursor.row > 0) {
        nextCursor.row--;
    }
    else if (message.direction === "down" && gameState.cursor.row < gameState.puzzle.height - 1) {
        nextCursor.row++;
    }
    else if (message.direction === "left" && gameState.cursor.col > 0) {
        nextCursor.col--;
    }
    else if (message.direction === "right" && gameState.cursor.col < gameState.puzzle.width - 1) {
        nextCursor.col++;
    }
    else {
        // Stay put.
    }
    let targetSquare = gameState.puzzle.grid[nextCursor.row][nextCursor.col];
    if (targetSquare.type == "block") {
        // TODO: skip over the block
        return;
    }
    else {
        nextCursor.blank = gameState.puzzle.blanks.get(`blank-${nextCursor.row}-${nextCursor.col}`);
    }
    Board.update(nextCursor);
    gameState.cursor = nextCursor;
}
function switchDirection(message) {
    let nextCursor = Object.assign({}, gameState.cursor);
    if (gameState.cursor.active === "across") {
        nextCursor.active = "down";
    }
    else {
        nextCursor.active = "across";
    }
    gameState.cursor = nextCursor;
    Board.update(gameState.cursor);
}
// TODO: maybe these should be a property of Direction?
function forwards(d) {
    return (d == "across") ? "right" : "down";
}
function backwards(d) {
    return (d == "across") ? "left" : "up";
}
function fillCell(message) {
    const b = gameState.cursor.blank;
    b.text = message.value;
    // e.guess = message.guess;
    b.correct = b.text == b.cell.solution; // TODO: does typescript do properties like python?
    Blanks.enter(b);
    publish("move", {
        from: gameState.cursor,
        direction: forwards(gameState.cursor.active)
    });
}
function backspaceCell(message) {
    let b = gameState.cursor.blank;
    b.text = "";
    b.correct = false;
    Blanks.clear(b);
    publish("move", {
        from: gameState.cursor,
        direction: backwards(gameState.cursor.active)
    });
}
function isEmpty(blank) {
    return !blank.text;
}
function isUnfinished(entry) {
    return entry.blanks.some(isEmpty);
}
function nextUnfinishedEntry(cursor, reverse) {
    // the next unfinished entry is:
    // the least entries away from the cursor
    // in the current direction
    // with any blank Blanks
    const currentEntryId = cursor.blank.entries[cursor.active].id;
    const entries = [...gameState.puzzle.entries.values()];
    if (reverse) {
        entries.reverse();
    }
    const currentIndex = entries.findIndex(k => k.id == currentEntryId);
    for (let i = currentIndex + 1; i != currentIndex; i = (i + 1) % entries.length) {
        if (isUnfinished(entries[i])) {
            return entries[i];
        }
    }
    return entries[currentEntryId];
}
function goto(blank) {
    const nextCursor = Object.assign({}, gameState.cursor);
    nextCursor.col = blank.cell.col;
    nextCursor.row = blank.cell.row;
    nextCursor.blank = blank;
    // TODO: which direction should be active?
    gameState.cursor = nextCursor;
    Board.update(nextCursor);
}
function goToNextEmptyBlank(message) {
    const target = nextUnfinishedEntry(gameState.cursor, message.reverse).blanks.find(isEmpty);
    goto(target);
}
function select(message) {
    const id = `blank-${message.target.row}-${message.target.col}`;
    if (gameState.cursor.blank.id == id) {
        switchDirection({});
        return;
    }
    goto(gameState.puzzle.blanks.get(id));
}
subscribe("select", select);
subscribe("move", move);
subscribe("next", goToNextEmptyBlank);
subscribe("switch", switchDirection);
subscribe("fill", fillCell);
subscribe("backspace", backspaceCell);
subscribe("noop", () => { });
window.addEventListener('load', initializePlaceholder); // TODO: remove
window.addEventListener('keydown', handleKeydown);
const inputElement = document.getElementsByTagName("input")[0];
inputElement.addEventListener("change", handleFileInput, false);
customElements.define("virtual-keyboard", VirtualKeyboard);
function showVirtualKeyboard() {
    return matchMedia("(pointer: coarse)").matches;
}
if (showVirtualKeyboard()) {
    document.getElementsByTagName("body")[0]
        .appendChild(document.createElement("virtual-keyboard"));
}
window["gs"] = gameState;
