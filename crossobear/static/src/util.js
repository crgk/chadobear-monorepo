export function range(n) {
    return Array.from(Array(n).keys());
}
export function clearChildren(el, keep = 0) {
    while (el.children.length > keep) {
        el.removeChild(el.lastChild);
    }
}
const subscriptions = {};
export function subscribe(command, callback) {
    var _a;
    let subs = (_a = subscriptions[command]) !== null && _a !== void 0 ? _a : [];
    subs.push(callback);
    subscriptions[command] = subs;
}
export function publish(command, message) {
    for (const sub of subscriptions[command]) {
        sub(message);
    }
}
