export class LandingSplash extends HTMLElement {
    connectedCallback() {
        const shadow = this.attachShadow({ mode: "open" });
        const header = document.createElement("h1");
        header.innerText = "Welcome!";
        shadow.appendChild(header);
        this.addEventListener("pointerup", this.remove);
    }
}
