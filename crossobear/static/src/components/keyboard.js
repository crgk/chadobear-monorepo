import { publish } from '../util.js';
export class VirtualKeyboard extends HTMLElement {
    constructor() {
        super(...arguments);
        this.symbols = [
            ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"],
            ["", "A", "S", "D", "F", "G", "H", "J", "K", "L"],
            ["", "<", "Z", "X", "C", "V", "B", "N", "M", ">"]
        ];
    }
    createKey(symbol) {
        const k = document.createElement("div");
        if (!symbol) {
            k.classList.add("virtual-keyboard-spacer");
            return k;
        }
        k.classList.add("virtual-keyboard-key");
        k.setAttribute("data-symbol", symbol);
        k.innerText = symbol;
        k.addEventListener("pointerup", handler);
        function handler(event) {
            const s = event.target.getAttribute("data-symbol");
            if (s == ">") {
                // TODO go right
            }
            else if (s == "<") {
                // TODO go left
            }
            else {
                publish("fill", {
                    value: s,
                });
            }
        }
        return k;
    }
    stylesheet() {
        const sheet = new CSSStyleSheet();
        sheet.replaceSync(`
            .virtual-keyboard {
                position: fixed;
                display: flex;
                flex-direction: column;
                bottom: 0;
                width: 100%;
                height: 200px;
            }

            .virtual-keyboard-row {
                display: flex;
            }

            .virtual-keyboard-key {
                font-family: sans-serif;
                text-color: white;
                background-color: lightblue;
                border-radius: 6px;
                margin: 5px;
                height: 40px;
                width: 40px;
                font-size: x-large;
                text-align: center;
                padding-top: 10px;
                user-select: none;
                -webkit-user-select: none;
            }
            
            .virtual-keyboard-spacer {
                width: 20px;
            }
        `);
        return sheet;
    }
    connectedCallback() {
        const shadow = this.attachShadow({ mode: "open" });
        const container = document.createElement("div");
        container.classList.add("virtual-keyboard");
        for (const symbolRow of this.symbols) {
            const row = document.createElement("div");
            row.classList.add("virtual-keyboard-row");
            for (const symbol of symbolRow) {
                row.appendChild(this.createKey(symbol));
            }
            container.appendChild(row);
        }
        shadow.appendChild(container);
        shadow.adoptedStyleSheets = [this.stylesheet()];
    }
}
