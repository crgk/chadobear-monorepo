import json
import re
from mastodon import Mastodon
from mastodon.streaming import CallbackStreamListener

BOT_ID = 112134987748584591
DRY_RUN = False
SILENT = False

# TODO this should be DB stuff
with open("gemaccounts.json") as accounts_file:
    accounts = json.load(accounts_file)

# TODO this shouldn't be global
masto = Mastodon(
    access_token="token.secret",
    api_base_url="https://botsin.space"
)
masto.status_favorite = masto.status_favourite


def reply(*args, **kwargs):
    if not SILENT:
        masto.status_reply(*args, untag=True, **kwargs)


def open_account(mention):
    mentioner_id = str(mention["status"]["account"]["id"])
    if mentioner_id in accounts:
        reply(mention["status"], "You already have an account. You can check your balance by saying \"check balance\".")
        return
    # create the new account and add it to the in-memory "db"
    new_account = {"balance": 1, "acct": mention["status"]["account"]["url"]}
    accounts[mentioner_id] = new_account

    # write a response to the user
    balance = new_account["balance"]
    currency_text = "ChadGem" if balance == 1 else "ChadGems"
    reply(mention["status"], "Your account is now open. You currently have %d %s." % (balance, currency_text))


def check_balance(mention):
    mentioner_id = str(mention["status"]["account"]["id"])
    try:
        checking = accounts[mentioner_id]
    except KeyError:
        reply(mention["status"], "I don't recognize you. If you'd like to open a ChadGem account with me, say \"open account\" first.")
        return
    # reply with user's balance
    balance = checking["balance"]
    currency_text = "ChadGem" if balance == 1 else "ChadGems"
    reply(mention["status"], "You currently have %d %s." % (balance, currency_text))


def give_gems_from_chad(mention):
    print("chad is giving out gems")

    content = mention["status"]["content"].lower()
    match = re.search(r"(\d) chadgems", content)    
    if not match:
        n = 1
    else:
        n = int(match.group(1))

    # identify the recipient(s)
    for mm in mention["status"]["mentions"]:
        if mm.id == BOT_ID:
            continue
        recipient_id = str(mm.id)
        recipient = accounts.get(recipient_id, {"acct": mm["url"], "balance": 0})

        print("%s (%s) is getting %d gems" % (mm["url"], recipient_id, n))
        updated_recipient = dict(recipient)
        updated_recipient["balance"] = recipient["balance"] + n
        accounts[recipient_id] = updated_recipient
    save_state()

def handle_mention(mention):
    print("handling a mention!")
    print(mention)
    # what if they ask for more than one action?
    text = mention["status"]["content"].lower()

    if "open account" in text:
        open_account(mention)
    elif "check balance" in text:
        check_balance(mention)
    elif "give" in text and mention["account"]["acct"] == "chad@beige.party":
        give_gems_from_chad(mention)
    else:
        reply(mention, "I don't know what to do with that. I really only understand two things: 'open account' and 'check balance'.")

    masto.status_favorite(mention["status"]["id"])


def handle_mentions(mentions):
    for mention in mentions:
        handle_mention(mention)


def save_state():
    if DRY_RUN:
        print("not saving state during dry run")
        return
    with open('gemaccounts.json', 'w') as accounts_outfile:
        json.dump(accounts, accounts_outfile)

listener = CallbackStreamListener(notification_handler=handle_mention)
print("Listening for notifications...")
masto.stream_user(listener)
