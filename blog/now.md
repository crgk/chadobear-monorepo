---
layout: page
title: Now
permalink: /now/
---

I currently live with my wife, two daughters, and two cats on Oahu.

I'm a full-time caregiver, starting to imagine what it will be like to have a job again.

2025 is a year of establishing new rhythms.
