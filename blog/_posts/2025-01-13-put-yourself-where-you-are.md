---
title: Putting Myself Where I Am
teaser: Some thoughts about presence and remoteness
---

> This was originally a private note I wrote on the beach on Wednesday, May 5, 2021.

I’ve learned, or maybe realized, an important thing about myself today. Even living in Hawaii, a place that still stuns me and feels like paradise, I need to make an effort to really be here.

For thirty years, I’ve been an indoor cat. Even if I’m not inside my home, I’m usually very much inside my own head. My hobbies are internal - reading, watching, listening, imagining. Left to my own devices (especially those with an internet connection) I will pull inward.

The life I’m living here highlights the biggest risk of my work life: the nowhereness. I’m on my couch, but in a meeting. It’s 5am, but time for work. My mind is following half a dozen conversations across as many slack channels.

None of this changed when I moved my body to Hawaii. In order to be here - and only here - I have to intentionally put myself here.

This is why I love to travel. When I visit somewhere, I stop being everywhere/nowhere and put myself there. In the chosen place. Among the other people who have put themselves there. And that’s the feeling that makes every place I visit into my new favorite place. That’s why I have so many more memories from vacations than just life. That’s why I like to fabricate occasions and latch on to events.

Being somewhere is the union of place and time. You’re always here, now. But I’m not always where I am.
