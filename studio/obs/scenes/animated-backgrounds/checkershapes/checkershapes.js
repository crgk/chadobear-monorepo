const params = new URLSearchParams(window.location.search)

const two = new Two({
	fullscreen: true,
	autostart: true
}).appendTo(document.body);

function randomSign() {
	return Math.sign(Math.random() - 0.5)
}

function isLandscape() {
	return window?.innerWidth > window?.innerHeight;
}

const palette = {
	offblack: 'rgb(0, 0, 0, .7)',
	white: 'rgb(255, 255, 255)',
	magenta: 'rgb(255, 0, 255)',
	yellow: 'rgb(255, 255, 0)',
	background: {
		green: 'rgb(0, 120, 60)',
		blue: 'rgb(0, 0, 120)',
		pink: 'rgb(240, 60, 240)',
		brightgreen: 'rgb(0, 240, 90)'
	},
	mclaren: {
		blue: 'rgb(62, 192, 252)',
		orange: 'rgb(255, 117, 0)',
		gray: 'rgb(73, 76, 80)'
	},
	joycon: {
		red: 'rgb(255, 69, 84)',
		blue: 'rgb(0, 195, 227)'
	},
	winter: {
		sky: 'rgb(54, 48, 98)',
		cloud: 'rgb(172, 188, 255)',
		snow: 'rgb(174, 226, 255)',
		ice: 'rgb(230, 255, 253)'
	}
}
palette.cyan = palette.mclaren.blue
const SHAPE_SIZER = 80

function getBackgroundGradientPoints() {
	if (isLandscape()) {
		// In Landscape mode, always return a straight vertical
		return { start: new Two.Vector(0, -two.height/2),
				 end: new Two.Vector(0, two.height/2) }
	}
	// Otherwise, we do a left-to-right for portrait
	let start = new Two.Vector(-two.width/2, 0)
	let end = new Two.Vector(two.width/2, 0)

	// And we rotate that based on the CYCLES OF THE MOON
	const rads = LunarPhase.Moon.lunarAgePercent() * (2*Math.PI)
	start.rotate(rads)
	end.rotate(rads)
	return { start: start,
			 end: end }
}

const bgStops = {
	og: [
		new Two.Stop(0, palette.background.pink, .8),
		new Two.Stop(.6, palette.mclaren.blue, .8),
		new Two.Stop(1, palette.background.brightgreen, .8)
	],
	winter: [
		new Two.Stop(0.0, palette.winter.sky),
		new Two.Stop(0.6, palette.winter.cloud),
		new Two.Stop(0.8, palette.winter.snow),
		new Two.Stop(1.0, palette.winter.ice)
	],
	christmas: [
		new Two.Stop(0.4, 'rgb(205, 0, 26)'),
		new Two.Stop(0.8, 'rgb(243, 243, 243)'),
		new Two.Stop(1.0, 'rgb(48, 183, 0)')
	]
}

function makeBgGradient() {
	const bgvectors = getBackgroundGradientPoints()
	let bggradient = two.makeLinearGradient(
		bgvectors.start.x, bgvectors.start.y,
		bgvectors.end.x, bgvectors.end.y,
		...bgStops.og
	)
	bggradient.units = 'userSpaceOnUse'
	return bggradient;
}

/**
 *
 * ASSORTED GRADIENTS
 *
 */
let bigradient = two.makeLinearGradient(0, -SHAPE_SIZER/2, 0, SHAPE_SIZER/2,
	new Two.Stop(0.2, palette.cyan, 1),
	new Two.Stop(1, palette.magenta, 1)
)
bigradient.units = 'userSpaceOnUse'

let pggradient = two.makeLinearGradient(0, -SHAPE_SIZER/2, 0, SHAPE_SIZER/2,
	new Two.Stop(0, palette.background.pink, 1),
	new Two.Stop(1, palette.background.brightgreen, 1)
)
pggradient.units = 'userSpaceOnUse'

let cygradient = two.makeLinearGradient(0, -SHAPE_SIZER/2, 0, SHAPE_SIZER/2,
	new Two.Stop(0.4, palette.cyan, 1),
	new Two.Stop(1, palette.white, 1)
)
cygradient.units = 'userSpaceOnUse'

let mggradient = two.makeLinearGradient(0, -SHAPE_SIZER, 0, SHAPE_SIZER,
	new Two.Stop(0, palette.magenta, 1),
	new Two.Stop(0.8, palette.white, 1)
)
mggradient.units = 'userSpaceOnUse'

let ylgradient = two.makeLinearGradient(0, -SHAPE_SIZER/2, 0, SHAPE_SIZER/2,
	new Two.Stop(0, palette.yellow, 1),
	new Two.Stop(0.6, palette.white, 1)
)
ylgradient.units = 'userSpaceOnUse'

let mcgradient = two.makeLinearGradient(0, -SHAPE_SIZER/2, 0, SHAPE_SIZER/2,
	new Two.Stop(0.3, palette.mclaren.blue, 1),
	new Two.Stop(1, palette.mclaren.orange, 1)
)
mcgradient.units = 'userSpaceOnUse'


/**
 * Background checkers
 */
let background = two.makeRectangle(two.width/2, two.height/2, two.width, two.height)
background.noStroke()
background.fill = makeBgGradient()

const SQLEN = 30
const SQADJ = 15

blackSquares = []
for (let i=0; i < two.width/SQLEN; i++) {
	for (let j=0; j < two.height/SQLEN; j++) {
		if (i % 2 == j % 2) {
			blackSquares.push(two.makeRectangle(i*SQLEN+SQADJ, j*SQLEN+SQADJ, SQLEN, SQLEN))
		}
	}
}
var chessboard = two.makeGroup(blackSquares)
chessboard.noStroke()
chessboard.fill = palette.offblack
chessboard.name = 'chessboard'

const shapeStrokes = [
	'white',
	'white',
	'white',
	'white',
	'white',
	'white',
	'white',
	'white',
	palette.mclaren.blue,
	palette.mclaren.blue,
	palette.magenta,
	palette.magenta,
	palette.magenta,
	cygradient,
	cygradient,
	cygradient,
	mggradient,
	mggradient,
	palette.background.brightgreen,
	palette.background.brightgreen,
	bigradient,
	bigradient,
	bigradient,
	bigradient,
]

const NSHAPES = params.get("numShapes") || 36
const MIN_SHAPE_SIZE = 40

let shapeGroup = two.makeGroup()
function makeShape(atEdge) {
	let sides = Math.ceil(Math.random() * 8)
	let size = Math.max(Math.random() * SHAPE_SIZER, MIN_SHAPE_SIZE)
	let stroke = Math.floor(Math.random() * shapeStrokes.length)
	let s = two.makePolygon(0, 0, size, sides)
	s.noFill()
	s.stroke = shapeStrokes[stroke]
	s.linewidth = 4

	if (atEdge) {
		let edge = Math.random() * 4; 
		if (edge > 3) {
			// left edge
			s.translation.x = 0 - size
			s.translation.y = Math.random() * two.height
		} else if (edge > 2) {
			// top edge
			s.translation.x = Math.random() * two.width
			s.translation.y = 0 - size
		} else if (edge > 1) {
			// right edge
			s.translation.x =  two.width + size
			s.translation.y = Math.random() * two.height
		} else {
			// bottom edge
			s.translation.x = Math.random() * two.width
			s.translation.y = two.height + size
		}
	} else {
		s.translation.x = Math.random() * two.width
		s.translation.y = Math.random() * two.height
	}

	s.velocity = {
		x: Math.random() * 0.4 * randomSign(),
		y: Math.random() * 0.4 * randomSign(),
		r: Math.random() * 0.005 * randomSign()
	}
	shapeGroup.add(s)
	return s
}

let shapes = []
for (let i=0; i < NSHAPES; i++) {
	shapes.push(makeShape(false))
}


two.bind('update', function(frameCount) {

	// Rotate the background when in portrait mode
	// but only do it every so often because it's a slow calculation
	if (!isLandscape()) {
		if (frameCount % 600 == 0) {
			background.fill = makeBgGradient()
		}
	}

	// remove shapes that are out-of-bounds
	shapes = shapes.filter(s => {
		if (s.center().position.x < 0 - SHAPE_SIZER || s.center().position.x > two.width + SHAPE_SIZER) {
			s.remove()
			return false
		} else if (s.center().position.y < 0 - SHAPE_SIZER || s.center().position.y > two.height + SHAPE_SIZER) {
			s.remove()
			return false
	  }
		return true
	})
	// make shapes to fill the array
	let i = shapes.length + 0
	for (; i < NSHAPES; i++) {
		shapes.push(makeShape(true))
	}
	// move every shape
	shapes.forEach(s => {
		s.translation.x += s.velocity.x
		s.translation.y += s.velocity.y
		s.rotation += s.velocity.r
	});
});

