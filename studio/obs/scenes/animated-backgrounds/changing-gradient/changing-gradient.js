// Initialize an instance to render
// render to the screen. Try changing
// the type property to:
//   Two.Types.canvas
//   Two.Types.webgl
// To see different rendering styles.
const two = new Two({
  type: Two.Types.svg,
  fullscreen: true,
  autostart: true
}).appendTo(document.body);

// Cache the center of the canvas
// for positioning objects.
const cx = two.width / 2;
const cy = two.height / 2;

// Define a list of colors for
// the linear gradient to cycle
// through.
const colors = [
  'red',
  'orange',
  'yellow',
  'green',
  'blue',
  'indigo',
  'violet'
];
// Define the index of the
// current color the gradient
// is starting from.
let gradient = makeGradient(colors)
gradient.units = 'userSpaceOnUse';

// Gradients are effects and can be applied
// to either the fill or the stroke of
// an Two.Path or other object. Make a rectangle
// to be filled by the gradient.
const rectangle = two.makeRectangle(cx, cy, two.width, two.height);
rectangle.noStroke();
rectangle.fill = gradient;

// Two.js event handlers
// to listen when the page
// resizes and the animation
// updates.
two.bind('update', update);

function makeStops(colorArray) {
	const n = colorArray.length

	return colorArray.map((c, i) => {
		const step = 1.0/n;
		return new Two.Stop(step * i, c)
	})
}

function makeGradient(colorArray) {
  // 4 component values used to
  // create a vertical line.
  let x1 = 0;
  let y1 = - cy;
  const x2 = 0;
  const y2 = cy;

  const arr = new Array(18).fill(colorArray).flat();

  let gradient = two.makeLinearGradient(x1, y1, x2, y2, ...makeStops(arr))
  gradient.units = 'userSpaceOnUse';
  return gradient
}

// Animate the scene
function update(frameCount) {

// Cache the center of the canvas
// for positioning objects.
const cx = two.width / 2;
const cy = two.height / 2;
  
  const cRes = 2000
  let cir = two.makeCircle(cx, cy, cx, cRes)
  two.remove(cir)

  let c = cir.toObject()
  
  gradient = rectangle.fill;
  gradient.left.x = c.vertices[two.frameCount % cRes].x
  gradient.left.y = c.vertices[two.frameCount % cRes].y
  gradient.right.x = c.vertices[(two.frameCount + cRes/2) % cRes].x,
  gradient.right.y = c.vertices[(two.frameCount + cRes/2) % cRes].y

}
