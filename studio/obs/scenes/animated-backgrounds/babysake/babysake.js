const params = new URLSearchParams(window.location.search)

const two = new Two({
	type: Two.Types.canvas,
	fullscreen: true,
	autostart: true
}).appendTo(document.body);

function randomSign() {
	return Math.sign(Math.random() - 0.5)
}

function isLandscape() {
	return window?.innerWidth > window?.innerHeight;
}

const palette = {
	babypowder: 'rgb(252, 249, 240)',
	blue: 'rgb(167, 188, 237)',
	green: 'rgb(150, 187, 80)',
	yellow: 'rgb(248, 218, 98)',
	purple: 'rgb(177, 118, 189)'
}

var colors = [ palette.purple, palette.blue, palette.green, palette.yellow ];

var container = two.makeGroup();

var rows = 20;
var cols = 9;
var width = 64;

for (var i = 0; i < rows; i++) {

  var even = !!(i % 2);
  var vi = i / (rows - 1);

  for (var j = 0; j < cols; j++) {

    var k = j;

    if (even) {
      k += 0.5;
      if (j >=  cols - 1) {
        continue;
      }
    }

    var hi = k / (cols - 1);

    var color = colors[Math.floor(Math.random() * colors.length)];

    var shape;
    if (Math.random() > 0.7) {
      shape = makeSquiggle(width, width / 3, Math.floor(Math.random() * 3) + 3, two);
      shape.noFill();
      shape.stroke = color;
      shape.linewidth = 4;
      shape.cap = 'round';
    } else {
      shape = new Two.Polygon(0, 0, width / 2, Math.floor(Math.random() * 3) + 3);
      shape.noStroke();
      shape.fill = color;
    }

    shape.rotation = Math.floor(Math.random() * 4) * Math.PI / 2 + Math.PI / 4;
    shape.translation.set(hi * two.width, vi * two.height);

    shape.step = (Math.floor(Math.random() * 8) / 8) * Math.PI / 120;
    shape.step *= Math.random() > 0.5 ? - 1 : 1;

    container.add(shape);

  }

}

two.bind('update', update);

function update() {

  for (var i = 0; i < container.children.length; i++) {
    var child = container.children[i];
    if (child.name === 'background') {
      continue;
    }
    child.rotation += child.step;
  }

}

function makeSquiggle(width, height, phi, two) {

  var amt = 64;
  var anchors = [];

  for (var i = 0; i < amt; i++) {
    var pct = i / (amt - 1);
    var theta = pct * Math.PI * 2 * phi + Math.PI / 2;
    var x = width * pct - width / 2;
    var y = height / 2 * Math.sin(theta);
    anchors.push(new Two.Anchor(x, y));
  }

  return two.makeCurve(anchors, true);

}

function makeRainbow(width, height, two) {

  var amt = 64;
  var anchors = [];

  for (var i = 0; i < colors.length; i++) {
  }

  return two.makeArcSegment(0, 0, 2, 4, 0, 180, 60);

}
