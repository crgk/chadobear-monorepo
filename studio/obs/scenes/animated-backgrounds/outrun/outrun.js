const params = new URLSearchParams(window.location.search)

const two = new Two({
	fullscreen: true,
	autostart: true
}).appendTo(document.body);

function randomSign() {
	return Math.sign(Math.random() - 0.5)
}

function isLandscape() {
	return window?.innerWidth > window?.innerHeight;
}

const palette = {
	offblack: 'rgb(0, 0, 0, .7)',
	white: 'rgb(255, 255, 255)',
	magenta: 'rgb(255, 0, 255)',
	yellow: 'rgb(255, 255, 0)',
	darkblue: 'rgb(0, 0, 120)',
	pink: 'rgb(240, 60, 240)',
	red: 'rgb(240, 0, 60)',
	orange: 'rgb(240, 160, 0)',
	green: 'rgb(0, 120, 60)',
	brightgreen: 'rgb(0, 240, 90)',
	darkpurple: 'rgb(48, 24, 72)',
	darkerpurple: 'rgb(12, 6, 36)',
}

function makeBgGradient() {
	let x = two.width/12;
	let y = two.height*.5;
	let r = two.width*.66;
	let bggradient = two.makeRadialGradient(
		x, y, r,
		new Two.Stop(0, palette.yellow, 1),
		new Two.Stop(0.5, palette.orange, 1),
		new Two.Stop(0.6, palette.red, 1),
		new Two.Stop(0.8, palette.pink, 1),
		new Two.Stop(1, palette.darkblue, 1)
	)
	bggradient.units = 'userSpaceOnUse'
	return bggradient;
}

function makeSurfaceGradient() {
	let x = two.width/12;
	let y = -two.height*.5;
	let r = two.width*.66;
	let surfacegradient = two.makeRadialGradient(
		x, y, r,
		new Two.Stop(0, palette.darkpurple, 1),
		new Two.Stop(0.7, palette.darkpurple, 1),
		new Two.Stop(0.9, palette.darkerpurple, 1)
	)
	surfacegradient.units = 'userSpaceOnUse'
	return surfacegradient;
}

/**
 * Background elements
 */
let background = two.makeGroup()
let sky = two.makeGroup()
let ground = two.makeGroup()
background.add(sky, ground)

let sunset = two.makeRectangle(two.width/2, two.height*.25, two.width, two.height/2)
sunset.noStroke()
sunset.fill = makeBgGradient()
const SQLEN = 30
const SQADJ = 15
let blackSquares = []
for (let i=0; i < two.width/SQLEN; i++) {
	for (let j=0; j < two.height/2/SQLEN; j++) {
		if (i % 2 != j % 2) {
			blackSquares.push(two.makeRectangle(i*SQLEN+SQADJ, j*SQLEN+SQADJ, SQLEN, SQLEN))
		}
	}
}
var chessboard = two.makeGroup(blackSquares)
chessboard.noStroke()
chessboard.fill = palette.offblack
sky.add(sunset)
sky.add(chessboard)


let surface = two.makeRectangle(two.width/2, two.height*.75, two.width, two.height/2)
surface.noStroke()
surface.fill = makeSurfaceGradient();
let horizon = two.makeLine(0, two.height/2, two.width, two.height/2);
horizon.stroke = palette.pink;
horizon.linewidth = 1;
ground.add(surface, horizon)

function drawLatitudeLines(step) {
	let lines = []
	const mid = two.width * 0.6;
	for (let l=1; l <= 12; l++) {
		let start = l * two.width/13;
		let dir = 1, offset = 0;
		if (start < mid) {
			dir = -1;
			offset = mid - start;
		} else {
			dir = 1;
			offset = start - mid;
		}
		// dir: shifts right or left (adds or substracts from start pos)
		// the first line to the left of the center
		let finish = start + (dir * offset) / 2;
		let line = two.makeLine(start, two.height/2, finish, two.height);
		line.stroke = palette.pink
		line.linewidth = 2
		line.className = "groundline"
		lines.push(line)
	}
	return lines;
}

colors = [
	"red", "orange", "yellow", "green", "blue", "purple"
]

function drawHorizonLine() {
	let base = two.height/2
	let line = two.makeLine(0, 0, two.width, 0);
	line.translation.y = base;
	line.stroke = palette.pink 
	line.linewidth = 2
	line.className = "groundline"
	return line;
}

horizonLines = two.makeGroup()
horizonLines.add(drawHorizonLine())
drawLatitudeLines()

function stepLines(frameCount) {
	ll = horizonLines.children;
	for (let i=0; i < horizonLines.children.length; i++) {
		line = horizonLines.children.shift();
		step = 2 * (line.translation.y**2 / two.height**2)**3
		if (line.translation.y > two.height) {
			two.remove(line);
			horizonLines.add(drawHorizonLine());
		} else {
			line.translation.y += step;
			horizonLines.add(line);
		}
	}
}
for (let t=0; t<9; t++) {
	horizonLines.add(drawHorizonLine())
	for (let s=0; s<360; s++) {
		stepLines()
	}
}
two.bind('update', function(frameCount) {
	stepLines(frameCount)
});

