var two = new Two({
  type: Two.Types.svg,
  fullscreen: true,
  autostart: true
}).appendTo(document.body);

let X_SPEED = 3, Y_SPEED = 2;
let dvd = two.interpret(document.getElementById("smirk"))
dvd.backgroundColor = "white"
changeColor();

function changeColor() {
	let c = getColor();
	g = two.makeLinearGradient(
		0, 0, 6, 6,
		new Two.Stop(0, c, 1),
		new Two.Stop(1, c, 0.1)
	)
	g.units = 'userSpaceOnUse'
	dvd.children[1].fill = g
	dvd.children[1].stroke = c;
}

//Pick a random color in RGB format
function getColor(){
	const colors = [
		"#00d5fc",
		"#00ec87",
		"#fff800",
		"#ff4900",
		"#fc00fc",
		"#ffa400",
		"#fc00fc",
		"#005dfc"
	]
	return colors[Math.floor(Math.random()*colors.length)]
}

function party() {
	for (t = 60; t < 6000; t+=60) {
		setTimeout(changeColor, t)
	}
}

//Check for border collision
function checkHitBox(){
	let hitx = false, hity = false;
    if(dvd.position.x + (dvd.width+dvd.length-12) >= two.width || dvd.position.x <= -10){
        X_SPEED *= -1;
		hitx = true;
        changeColor();
    }
    if(dvd.position.y + (dvd.height+dvd.length-12) >= two.height || dvd.position.y <= -10){
        Y_SPEED *= -1;
		hity = true;
        changeColor();
    }

	if (hitx && hity) {
		// corner party!
		party()
	}
}

two.bind('update', function(frameCount) {
	// Move the logo
	dvd.position.x += X_SPEED;
	dvd.position.y += Y_SPEED;
	// Check for collision 
	checkHitBox();
});
