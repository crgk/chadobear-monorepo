
var two = new Two({
  type: Two.Types.svg,
  fullscreen: true
}).appendTo(document.body);


const palette = {
	offblack: 'rgb(0, 0, 0, .7)',
	white: 'rgb(255, 255, 255)',
	cyan: 'rgb(0, 255, 255)',
	magenta: 'rgb(255, 0, 255)',
	yellow: 'rgb(255, 255, 0)',
	background: {
		green: 'rgb(0, 120, 60)',
		blue: 'rgb(0, 0, 120)',
		pink: 'rgb(240, 60, 240)',
		brightgreen: 'rgb(0, 240, 90)'
	},
	mclaren: {
		blue: 'rgb(62, 192, 252)',
		orange: 'rgb(255, 117, 0)',
		gray: 'rgb(73, 76, 80)'
	}
}
palette.cyan = palette.mclaren.blue
const SHAPE_SIZER = 80


/**
 *
 * GRADIENTS
 *
 */
let bigradient = two.makeLinearGradient(0, -SHAPE_SIZER/2, 0, SHAPE_SIZER/2,
	new Two.Stop(0, palette.cyan, 1),
	new Two.Stop(1, palette.magenta, 1)
)
bigradient.units = 'userSpaceOnUse'

var scribsvg = document.querySelector('svg#scrib');
var scrib = two.interpret(scribsvg);

scrib.center();
scrib.position.x += 320;
scrib.position.y += 30;
scrib.scale = 3;
scrib.subdivide();
scrib.noFill();
scrib.stroke = bigradient

two.add(scrib);

var thankssvg = document.querySelector('svg#thanks');
var thanks = two.interpret(thankssvg);
thanks.center();
thanks.noFill();
thanks.scale = 4
thanks.stroke = palette.white
thanks.position.x += 340
thanks.position.y += 20
thanks.opacity = 0

two.bind('resize', resize);
resize();

two.bind('update', function(frameCount) {
	var xpct = frameCount / 30.0;
	if (scrib.ending < 1.1) {
		scrib.ending = xpct;
	} else if (frameCount > 60 && thanks.opacity < 1.0) {
		thanks.opacity += 0.02;
	} else if (frameCount > 120) {
	}
}).play();

function resize() {
  two.scene.position.set(two.width / 2, two.height / 2);
}
