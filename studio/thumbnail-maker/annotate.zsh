# TODO:
# - fetch latest screenshot
# - rotate through color pairs
# - output with better name
# - cleanup intermediary file?

get-color-pair() {
	sed -n "$(ls -l output | wc -l)p" color-codes.csv
}

get-font() {
	echo '/System/Library/Fonts/Supplemental/Arial Rounded Bold.ttf'
}

get-frame-image() {
	find ~/Screenshots -type f | sort -nr | head -n 1
}

get-date-display() {
	date "+%a %b %d %Y"
}

annotate-thumbnail() {
	readonly teaser=${1:?"We need a teaser text"}
	get-color-pair | IFS=, read -r background fill
	unset IFS
	magick -background "#$background" -fill "#$fill" -gravity center -font "$(get-font)" -size 625x330 caption:"$teaser" miff:- | magick composite -geometry +30+90 - "$(get-frame-image)" temp.png
	magick -background "#$background" -fill "#$fill" -font "$(get-font)" -size 1080x -gravity center label:"$(get-date-display)" miff:- | magick composite -geometry +605+723 - temp.png output/NYTXW-"$(date -I)".png
	rm temp.png
}

annotate-thumbnail $1
