const fs = require('fs');

async function getColorCodes() {
	const URL = "https://botsin.space/api/v1/accounts/248959/statuses?limit=40"
	var resp = await fetch(URL);
	var data = await resp.json();
	return data.map(d => d.tags.map(t => t.name)).filter(a => a.length == 2)
}

function handleFSError(err) {
	if (err) console.error(err);
}

function writeToFile(colorPairs) {
	const filename = "color-codes.csv"
	let csvContent = colorPairs.map(pair => pair.join(",")).join("\n");
	fs.appendFile(filename, csvContent + "\n", handleFSError);
	return colorPairs;
}

function log(pairs) {
	const logfile = "fetch-log.csv";
	let date = new Date();
	fs.appendFile(logfile, `${date.toISOString()},${pairs.length}\n`, handleFSError);
}

getColorCodes().then(writeToFile).then(log);
