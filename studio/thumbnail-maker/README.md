# thumbnail-maker

A collection of scripts and stuff to make it easier for me to create thumbnails for my YouTube videos.

## TODO:

- [ ] pull the color codes out of the post text instead of relying on tags (all numeric colors are lost because they don't look like hashtags)
- [ ] get the still frame from the video with ffmpeg
